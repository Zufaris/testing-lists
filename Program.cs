﻿using System.Collections;

namespace testing_Lists
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Girl girl = new Girl();
            List<Girl> girlsList = new List<Girl>();
            girlsList.Add(girl);
            girlsList.Add(new Girl("Silwia", 40, 70, 70, 70));
            girlsList.Add(new Girl("Beatris", 23, 95, 58, 92));

            girlsList.Sort();

            Hashtable hash = new Hashtable();
            foreach (Girl ein in girlsList) 
            {
                hash.Add(ein.Name, ein);
            }
            Console.WriteLine("Girls list:");
            foreach (string s in hash.Keys)
            {
                Console.WriteLine(s);
            }
            Console.WriteLine("Full info:");
            foreach (Object key in  hash.Keys)
            {
                Girl baby = (Girl)hash[key];
                Console.WriteLine(baby);
            }

        }
    }

    class Girl: IComparable<Girl> 
    {
        string name;
        byte age;
        byte breast;
        byte waist;
        byte hips;

        public string Name { get { return name; } set { name = value; } }
        public byte Age { get { return age; } set { age = value; } }
        public byte Breast { get { return breast; } set { breast = value; } }
        public byte Waist { get { return waist; } set { waist = value; } }
        public byte Hips { get { return hips; } set { hips = value; } }
        public Girl()
        {
            name = "Angela";
            age = 20;
            breast = 90;
            waist = 60;
            hips = 90;
        }

        public Girl(string name, byte age, byte breast, byte waist, byte hips)
        {
            this.name = name;
            this.age = age;
            this.breast = breast;
            this.waist = waist;
            this.hips = hips;
        }

        public override string ToString()
        {
            return $"Mrs {name} is {age} years old with " + ((breast > 89) ? "big" : "compact") + $" breast ({breast}), waist ({waist}) and " + ((hips < 90) ? "thin" : "realy good") + $" ass ({hips})";
        }

        public int CompareTo(Girl other)
        {
            return ((breast + hips).CompareTo(other.breast + other.hips));
        }
    }
}